create database Programming;
use Programming;

-- Cours la programmation sous Mysql (Programming sql)
-- I. Declaration
-- syntaxe
 --  declare nomvariablbe type(int, varchar,float,char,...);
 -- Le mot declare doit être écrit dans
 -- le bloc begin et end d'une fonction ou procedure stockée
drop procedure if exists P1;
delimiter $$
 create procedure P1()
 begin
        declare x,z int ;
        declare titre,nom varchar(40);
        declare dateEmbauche date;
        set x=8;
        set z=8;
        select x,z;   -- affiche la valeur de x et de y
 end$$
 call P1();
-- II. Affectation
     -- syntaxe 
         -- set nombvariable = valeur; 
         -- set nomvariable := valeur;
         set @r=9;    -- @r est une variable de session
         select @r;
-- III. Structure Alternative
    -- IF  ,  ELSEIF  ,  ELSE
    -- syntaxe :
    /*
        if condition1 then
           bloc 1;
        elseif condition2   then
            bloc 2;
        else
              bloc 3;
        end if;
    */
      -- La notion de Case  (éauivalent de switch)    
      
      -- syntaxe :
      
         case   
          when condition1 then  bloc1;
          when condition2 then bloc2;
          when condition3 then bloc3;
          else bloc4;		-- else est non obligatoire	
         
         end case;
    
-- Exemple  : Ecrire un programme sous Mysql pour comparer deux nombres
drop procedure if exists Comparer;
delimiter $$
create procedure Comparer(in a int, in b int)
begin
      declare resultat varchar(50);
      
      if a > b then
          set resultat := concat(a, ' est plus grand que ',b);
      elseif b>a then
          set resultat := concat(b, ' est plus grand que ',a);
      else
        set resultat := concat(a, ' est égal à ',b);
      end if;      
     select resultat; 
end$$
-- Le test de la procedure stockée comparer
call comparer(2,2);

-- *************************************************
drop procedure if exists Comparerbis;
delimiter $$
create procedure Comparerbis(in a int, in b int)
begin
      declare resultat varchar(50);
      
      case 
       when a > b  then
          set resultat := concat(a, ' est plus grand que ',b);
      when b>a then
          set resultat := concat(b, ' est plus grand que ',a);
      else
        set resultat := concat(a, ' est égal à ',b);
      end case;      
     select resultat; 
end$$
-- Exemple 1: Ecrire un programme sous Mysql pour comparer 3 valeurs
drop procedure if exists comparaison;
delimiter $$
create procedure comparaison(in a int,in b int,in c int)
begin
    declare res varchar(40);
        case 
             when a=b and a=c then set res=concat('les valeurs sont égales');
             when a>=b and a>=c then  set res=concat(a ,' est plus grand');
             when b>=a and b>=c then  set res=concat(b ,' est plus grand');
             else 
                      set res=concat(c ,' est plus grand');		
        end case;
        SELECT RES;
end $$
-- **********************************************
call comparaison(40,4,40);

-- ***********************************
-- Exemple 2: Ecrire un programme sous mysql qui affiche la mention 
-- des élévés selon leur moyenne obtenue 
      -- (echec si moyenne < 10)
      -- (passable  10<=moyenne<12)
      -- (A.bien     12 <=moyenne< 14)
      -- (bien     14 <=moyenne< 16)
      -- (Trés.bien     14 <=moyenne< 18)
      -- (au dela de 18 Excellent)
-- ****************************************************
drop procedure if exists Proc_Mention;
delimiter $$
create procedure Proc_Mention(in moy float,out mention varchar(40))
begin
         case
         when moy<10 and moy >=0 then set mention='Echec';
         when moy>=10 and moy<12 then set mention='Passable';
         when moy>=12 and moy<14 then set mention='A.bien';
          when moy>=14 and moy<16 then set mention='Bien';
          when moy>=16 and moy<18  then set mention='Trés.Bien';
          when moy>=18 and moy<=20 then set mention='Excellent';
          else
            set mention='Pas de mention';
         end case;
end$$

-- ****************************************
  set @reponse='';
  set @moy=-8;
  call Proc_Mention(@moy,@reponse);
  select @moy as note,@reponse as mention;
-- *********************************************************************
drop function if exists Funct_Mention;
delimiter $$
create Function Funct_Mention(moy float) returns  varchar(40)
 deterministic
begin
          declare mention varchar(40);
         case
         when moy<10 and moy >=0 then set mention='Echec';
         when moy>=10 and moy<12 then set mention='Passable';
         when moy>=12 and moy<14 then set mention='A.bien';
          when moy>=14 and moy<16 then set mention='Bien';
          when moy>=16 and moy<18  then set mention='Trés.Bien';
          when moy>=18 and moy<=20 then set mention='Excellent';
          else
            set mention='Pas de mention';
         end case;
         return mention;
end$$
-- ******************************************
set @x=17;
select @x as 'note obtenue',Funct_Mention(@x) as 'Mention';
