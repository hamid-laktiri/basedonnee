-- EX1- Ecrire un Transact SQL qui permet de permuter 
-- les valeurs de deux variables
use tp7_dev202;
drop procedure if exists P1;
delimiter $$
create procedure P1(in a int,in b int)
begin
         declare message varchar(256);
         declare c int;
       set message= concat('la valeur de a =',a,'  celle de b = ',b, ' avant permutation');
       
         set c=a;
         set a=b;
         set b=c;
         
       set message= concat(message,' -- ',
                       concat('la valeur de a =',a,'  celle de b = ',b, ' apres permutation'));
select message as 'resultat';
end $$
call p1(3,5);
-- *******************************************************************
-- EX2- Ecrire un code SQL qui permet de retourner la valeur maximale et la valeur
--  minimale entre les valeurs de trois variables.
use tp7_dev202;
drop procedure if exists P2;
delimiter $$
create procedure P2(in a int,in b int, in c int,out maxval int, out minval int)
begin
drop  temporary table if exists T;
create temporary table T(x int);   
insert into T values(a),(b),(c);
select max(x) into maxval from T;  
select  min(x) into minval from T;        
end $$
-- Test 
set @x =0;
set @y=0;
call P2(5,70,4,@x,@y);
select @x as Maxi ,@y as Mini;
-- ******************************************************************************
-- Ex3 -Ecrire le code SQL qui permet de retourner le factoriel d’une valeur.
use tp7_dev202;
drop procedure if exists P3;
delimiter $$
create procedure P3(in a int,out fact int)
begin
       declare i int;
       declare f int;
       set i=1;
       set f=1;
       if a >= 0 then
       while i<=a do
             set f= f*i;
             set i=i+1;
       end while;
      end if;
      select f into fact; 
end $$
-- ******************************************************************************
set @factoriel =-1;
set @x =7;
call P3(@x,@factoriel);
select concat('factoriel de ',@x, ' = ',@factoriel) as Message;
-- Ex4 - Ecrire un code SQL qui permet de retourner le nom d’une valeur comprise 
-- entre 0 et 9 ( si la valeur vaut 0 on affiche Zéro , 
-- si cette valeur vaut 1 on affiche Un, etc ……)
use tp7_dev202;
drop procedure if exists P4;
delimiter $$
create procedure P4(in a int,out msg varchar(30))
begin
drop  temporary table if exists T;
create temporary table T(x int,nom varchar(30));  
insert into T values (0,'Zéro'),(1,'Un') ,(2,'Deux'),(3,'Trois'),
                     (4,'Quatre'),(5,'Cinq'),(6,'six'),(7,'Sept'),
                     (8,'Huit'),(9,'Neuf');
if a >=0 then                     
       select nom into msg from T where x=a;  
else
        select concat ('- ',nom) into msg from T where x=abs(a);  
end if;
end $$
-- Test
set @nom ='';
set @v =-7;
call P4(@v,@nom);
select @v as valeur, @nom as nom;
 -- EX5 -Ecrire un code SQL qui permet d’afficher les nombre premiers 
 -- qui sont compris entre 1 et 20.
 use tp7_dev202;
drop procedure if exists P5;
delimiter $$
create procedure P5()
begin
    declare i,j  int;
    declare compteur int;
     drop temporary table if exists Premier; 
     create  temporary table Premier(x int);
     set i=1;
    -- while pour parcourir les nombres entre 1 et 20 (i)
    while  i<=20   do
      set compteur =0;
      set j=1;
          -- while pour parcourir les nombres entre j=1 à i  (j) pour chercher les multiples de i
           while  j <=i  do
                    -- if pour compter les multiples (j) de la variable i
                    if i % j =0 then
                        set compteur=compteur+1;
                    end if;
                    set j=j+1;
            end while;
                   if compteur =2 then
                         insert into Premier values(i);
				    end if;
       set i=i+1;
     end while;
      select x as Nombre_premier from premier;
end $$
-- Test
call P5();
   







