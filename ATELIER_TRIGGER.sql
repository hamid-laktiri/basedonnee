-- Utilisation des Triggers  ----
-- 1. La définition
/*
   un trigger est script sql qui se déclenche suite à l'événement
   Insert , ou Delete ou Update sur une table.
   - Un trigger s'applique sur une et une seule table.
   - Un trigger est un objet de la base de données (càd c'est un
      composant comme table,view,function , procedure ,.....
   - avant d'exécuter une opération I.U.D  le serveur vérifie
    si la table correspondante à cette mise à jour conteint
    un trigger.
*/
-- 2. Création d'un trigger
-- syntaxe: 
  /*
     use nom_Database;
     drop trigger if exists nom_trigger;
     delimiter $$
     create trigger nom_trigger time_evenement nom_evenement
     on nom_table for each row
     begin
             si une régle n'est pas respctée alors
                  annuler la transaction
             fsi
     enf $$
     
     -- time_evenement : before   after
     -- nom_evenement : insert , update , delete
     -- for each row : pour parcourir les lignes qui
					ont subis la transaction
     -- annuler la transaction :
		signal sqlstate '45000' set message_text='opération rejetée' 
     --  dans le bloc begin et end , on utilise les mots old et new pour accéder
         aux champs de la transaction insert (new) ,update (old, new)ou delete(old)         
  */
-- Exercice d'application
/*
    Créer une base de données DB_trigger_Dev202 , 
   -- puis Ajouter une table client (numclt,nomclt,Situation_Familiale_clt) avec
   le respect des ces régles en utilisant les triggers:
          -- numclt représente l'identifiant
          --nomclt obligatoire
          -- Situation faamiliale ('C','M','D','V')
   -- Ajouter une table commande (refcde,numclt,datecde)  avec
      le respect des ces régles en utilisant les triggers:
		  -- refcde doit être unique (identifiant)  et  non null
          -- numclt doit exister dans la table client (reference vers le client) , non null
          --date cde  obligatoire et >=now()			
*/
drop database if exists DB_trigger_Dev202;
create database DB_trigger_Dev202;
use DB_trigger_Dev202;
-- *******************************
create table client (numclt int  ,
                     nomclt varchar(30) ,
                     SF varchar(1) 
					);
create table commande (refcde varchar(10) primary key not null,
                      numclt int not null ,
                      datecde date not null,
                      foreign key (numclt) references client (numclt)
                      on delete cascade on update cascade)   ;  
-- *****************************
drop trigger if exists Trig_client_insert ;     
delimiter $$
create trigger Trig_client_insert before insert 
on client  for each row
begin
         declare compteur int;
         declare msg varchar(200);
         -- vérification de SF
       if (new.SF is null) or (new.SF not in ('C','M','D','V') ) then
		 signal sqlstate '45000' set message_text='la situation familiale  est érronée, veuillez saisir une de ces valeurs C ou,M ou D ou V';
       end if;
         -- vérification de nomclt
       if (new.nomclt is null) then
		 signal sqlstate '45000' set message_text='le nom est obligatoire';
       end if;
        if (new.numclt is null) then
		 signal sqlstate '45000' set message_text='le numéro client est obligatoire';
       end if;
        -- verification de unicité du numclt       
       select count(numclt) into compteur from client where numclt=new.numclt;
       if compteur = 1 then
		select concat('Ce numéro client',new.numclt,' est déja attribué ')
          into msg;
          signal sqlstate '45000' 
                    set message_text=msg;
       end if;
                
end $$
               
-- Trigger Update sur table client
drop trigger if exists trig_client_update;
delimiter $$
create trigger trig_client_update before update 
on client for each row
begin
      declare compteur int;
         declare msg varchar(200);
            -- vérification de SF
       if (new.SF is null) or (new.SF not in ('C','M','D','V') ) then
		 signal sqlstate '45000' set message_text='la situation familiale  est érronée, veuillez saisir une de ces valeurs C ou,M ou D ou V';
       end if;
       -- vérification de nomclt
       if (new.nomclt is null) then
		 signal sqlstate '45000' set message_text='le nom est obligatoire';
       end if;
        if (new.numclt is null) then
		 signal sqlstate '45000' set message_text='le numéro client est obligatoire';
       end if;
       -- verification de unicité du numclt       
       select count(numclt) into compteur from client where numclt=new.numclt;
       if compteur = 1 then
		select concat('Ce numéro client',new.numclt,' est déja attribué ')
          into msg;
          signal sqlstate '45000' 
                    set message_text=msg;
       end if;
       -- *******************************
       update commande set numclt=new.numclt where numclt=old.numclt;		
       
end $$
-- Création d'une table archive client

create table client_archive like client;
create table commande_archive like commande;

-- Trigger de l'evenement Delete sur la table client
drop trigger if exists trig_client_delete;
delimiter $$
create trigger trig_client_delete after delete
on client for each row
begin
	insert into client_archive values (old.numclt,old.nomclt,old.sf,now());
    delete from commande where numclt=old.numclt;
end $$
-- *************************************************************************
drop trigger if exists trig_commande_insert;
delimiter $$
create trigger trig_commande_insert before insert on commande for each row
begin
   -- Contrainte not null
  if new.refcde is null or new.numclt is null or new.datecde is null then
    signal sqlstate '45000' set message_text='La saisie de tous les champs est obligatoire';
 end if;
       -- contrainte identifiant
       
 if   (select count(refcde) from commande where refcde=new.refcde)=1 then
    signal sqlstate '45000' set message_text='Cette réference de la commande existe déja';
 end if;
 if   new.datecde < date(now()) then
    signal sqlstate '45000' 
         set message_text='La date commande doit être égale à aujourdhui ou plus tôt';
 end if;     
     if   (select count(numclt) from client where numclt=new.numclt)=0 then
    signal sqlstate '45000' set message_text='Le N° client de cette commande non trouvé';
 end if;
end $$
-- *************************************************************************
drop trigger if exists trig_commande_update;
delimiter $$
create trigger trig_commande_update before update on commande for each row
begin
   -- Contrainte not null
  if new.refcde is null or new.numclt is null or new.datecde is null then
    signal sqlstate '45000' set message_text='La saisie de tous les champs est obligatoire';
 end if;
       -- contrainte identifiant
       
 if   (select count(refcde) from commande where refcde=new.refcde)=1 then
    signal sqlstate '45000' set message_text='Cette réference de la commande existe déja';
 end if;
 if   new.datecde < date(now()) then
    signal sqlstate '45000' 
         set message_text='La date commande doit être égale à aujourdhui ou plus tôt';
 end if;     
     if   (select count(numclt) from client where numclt=new.numclt)=0 then
    signal sqlstate '45000' set message_text='Le N° client de cette commande non trouvé';
 end if;
end $$

-- ********************************
-- *************************************************************************
drop trigger if exists trig_commande_delete;
delimiter $$
create trigger trig_commande_delete after delete on commande for each row
begin
   insert into commande_archive values(old.refcde,old.numclt,old.datecde,now());  
end $$
-- Tests
select * from client
union
select * from client_archive;
insert into client(numclt,nomclt,SF) values(2,'Rami','M');
insert into client(numclt,nomclt,SF) values(1,'Saoud','C');
update client set SF='H' where numclt=2;
delete from client  where numclt=2;
select * from commande;
select * from commande_archive;
insert into commande values ('cde003',2,'2022/10/31');



