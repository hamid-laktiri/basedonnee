			-- requete sql , script sql ,transact sql
-- Langage de Manipulation des données
 -- create database DB01;
use DB01;
-- créer la table auteur(numaut,nom,pays)
/*
create table auteur(numaut int not null auto_increment ,
                    nom varchar(30) not null,
                    pays varchar(30) not null default 'Maroc',
                    primary key(numaut)
            );
*                                            /
-- créer la table livre(refliv,numaut ,titre ,prix)            
/*
create table livre(refliv varchar(10) not null primary key,
                   numaut int not null,
                   titre varchar(100) not null,
                   prix float not null default 0 check (prix >=0),
                   foreign key(numaut) references auteur(numaut)
                   on delete cascade on update cascade
                   );
 */
 -- drop table livre;
/*
create table livre(refliv varchar(10) not null ,
                   numaut int not null,
                   titre varchar(100) not null,
                   prix float not null );
alter table livre add constraint pk primary key(refliv);
alter table livre add constraint cst_prix check(prix >=0);
alter table livre modify column prix float not null default 0;
alter table livre add constraint fk foreign key(numaut) 
         references auteur(numaut)  on delete cascade on update cascade;

*/
-- Insertion des données dans la table auteur

insert into auteur (nom,pays)  
values('Najib Mahfoud','Egypte'),('Nizar kabani','Tunisie'),('Med Choukri','Maroc');
select * from auteur;
insert into Auteur (nom) values('Farhat'),('Mourchid');
-- Insertion des données dans la table Livre
insert into livre  values('Liv0001',1,'Quartier',100),
                         ('Liv0002',1,'le voleur et les chiens',150);
insert into livre(refliv,numaut,titre)  values('Liv0003',2,'Pain sec');
select * from livre;             
describe Livre;
-- Modification des données
-- modifier le pays de l'auteur N°4 par France
select * from auteur;

update auteur set pays='france' where numaut=4;
-- augmenter le prix des livres de 10% ecrits par nijib mahfoud
select * from livre;
update  livre inner join auteur on livre.numaut=auteur.numaut
set prix= prix *1.1  
where  nom ='najib mahfoud';
-- suppression des données 
-- supprimer le livre portant le titre xxxxx
delete from livre where titre='xxxxx' ;
select * from livre;
-- supprimer les livres ecrits par les auteurs français
insert into livre values('Liv00089',4,'TTTTT',30);
 delete livre 
 from livre inner join auteur on livre.numaut=auteur.numaut
 where auteur.pays ='france';
-- supprimer les livres ayant le prix entre 10 et 70
delete livre from livre where prix between 10 and 70;
-- les consultations des données (affichage)
/*
select   
      la liste des champs ou bien * ou les expressions de calcul (+,-,/,mod,*)
      ou les fonction agrégées (sum,count,max,min,avg)
from       les tables
where     conditions  
group by   champs de regroupement
having    condition sur les fonctions (min,max,avg,count,sum)
order by   champs de tri asc , desc 
*/
-- afficher la liste des auteurs trie par nom croissant
select *
from auteur  
order by nom desc;
-- afficher le nombre d'auteurs marocains
select count(numaut)  as 'Nbre Auteur'
from auteur
where pays='maroc';
-- afficher le plus petit   et le plus grand prix des livres
select  min(prix) as 'prix min',max(prix) as 'prix max'
from livre;
-- afficher le nombre de livre écrits par pays
select  pays,count(refliv) 
from livre inner join auteur on livre.numaut=auteur.numaut
group by pays;
/*---------------------------------------------------*/
select  pays,count(refliv) 
from livre left join auteur on livre.numaut=auteur.numaut
group by pays;
/*---------------------------------------------------*/
select  pays,count(refliv) 
from livre right join auteur on livre.numaut=auteur.numaut
group by pays;
-- afficher les pays des auteurs ayant plus de 1 livre
select  pays ,count(refliv)
from livre right join auteur on livre.numaut=auteur.numaut
group by pays
having count(refliv)>1;
--  les opérations arithmétiques en sql (+, -, *, /, mod)
select (2+5) ,(4*5),(6/2),(13 mod 2);
-- les fonctions de comparaison (=, >,<,>=,<=,<>,like, betwwen)
-- afficher les auteurs dont leur nom commence par 'N'
select * from auteur where nom like 'n%';
-- afficher les auteurs dont leur nom commence par 'N' et se termine par 'i'
select * from auteur where nom like 'n%i';
-- afficher les auteurs dont leur nom contient l'avant dernière lettre 'i' 
select * from auteur where nom like '%i_';
-- afficher les auteurs dont leur nom possèdela lettre 'r' en troisième position
select * 
from auteur
where nom like '__r%';  
 
















                   
                   
                   
                   



