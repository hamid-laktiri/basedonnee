from  pymongo import MongoClient
class Pilote :
        cnx=MongoClient(host="localhost",port=27017)
        db=cnx["DB_DEV202_1"]
        col=db['pilotes']
        def __init__(self,num=any,nom=any,ville=any,salaire=any):
            self.num=num
            self.nom=nom
            self.ville=ville
            self.salaire=salaire

        def  liste_pilotes (self) :
            # self.pilotes=list(Pilote.col.find())
            return list(Pilote.col.find({},{"_id":0}))
            # for p in self.pilotes :
            #    print(f"N° : {p['num']}  Nom : {p['nom']}  Salaire : {p['salaire']}")
        def recherche_par_num(self,num):
            return Pilote.col.find_one({"num":num},{"_id":0})
        def  infospilote(self):
             self.count=len(list(Pilote.col.find()))
             print(f"Nombre de pilotes :  {self.count}")
             self.totalsalaire=sum(list(map(lambda  p : p['salaire'],Pilote.col.find({},{'salaire' : 1}))))
             self.moysalaire = self.totalsalaire/self.count
             print(f"Total Salaire : {self.totalsalaire}   Moyen Salaire : {self.moysalaire}")

        def  ajouterPilotes(self):
            obj={'num':self.num,'nom':self.nom,'ville':self.ville,'salaire':self.salaire}
            Pilote.col.insert_one(obj)

        def  supprimerPilote(self, num):
              filter={"num":num}
              Pilote.col.delete_many(filter)
              self.liste_pilotes()





























