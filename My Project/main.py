import tkinter as tk
from pilotes.pilote import Pilote
from tkinter import ttk
from tkinter import messagebox
# position
def position(objs):
    for i in objs:
        i["obj"].grid(row=i["row"],column=i["col"],ipadx=15,ipady=8,pady=10)
# pilote objest
pilote1=Pilote()
# delete function
def delete_pilote():
    alert_window=tk.Toplevel()
    alert_window.grab_set()
    label=ttk.Label(alert_window,text="Enter le Numero du Pilote : ")
    label.grid(column=1,row=1)
    enry=ttk.Entry(alert_window,)
    enry.grid(column=2,row=1)
    def delete():
        ok=messagebox.askyesno("askyesno","Voulez vous supprimer ce pilote")
        if ok:
            pilote1.supprimerPilote(int(enry.get()))
            alert_window.destroy()

    btn_update=ttk.Button(alert_window,text="Delete",command=delete)
    btn_update.grid(column=2,row=3)
# add function
def add_pilote():
    addwindow=tk.Toplevel()
    addwindow.grab_set()
    # labels
    label_num=ttk.Label(addwindow,text="Numero:")
    label_nom=ttk.Label(addwindow,text="Nom:")
    label_ville=ttk.Label(addwindow,text="Ville:")
    label_salaire=ttk.Label(addwindow,text="Salaire:")
    # Entries
    num_entery=ttk.Entry(addwindow)
    nom_entery=ttk.Entry(addwindow)
    ville_entery=ttk.Entry(addwindow)
    salaire_entery=ttk.Entry(addwindow)
    
    def add():
        if num_entery.get()!="" and nom_entery.get()!="" and ville_entery.get()!=""\
                            and salaire_entery.get()!="":
            pilote1.num=int(num_entery.get())
            pilote1.nom=nom_entery.get()
            pilote1.ville=ville_entery.get()
            pilote1.salaire=salaire_entery.get()
            pilote1.ajouterPilotes()
            addwindow.destroy()
        else:
            messagebox.askretrycancel(message="vuillez remplir tous les champs")
    # buttons
    add=ttk.Button(addwindow,text="Add",command=add)
    cancel=ttk.Button(addwindow,text="Close",command=addwindow.destroy)
    position(
    [
    {"obj":label_num,"row":1,"col":1},
    {"obj":num_entery,"row":1,"col":2},
    {"obj":add_btn,"row":1,"col":3},
    {"obj":label_nom,"row":2,"col":1},
    {"obj":nom_entery,"row":2,"col":2},
    {"obj":label_ville,"row":3,"col":1},
    {"obj":ville_entery,"row":3,"col":2},
    {"obj":label_salaire,"row":4,"col":1},
    {"obj":salaire_entery,"row":4,"col":2},
    {"obj":add,"row":5,"col":3},
    {"obj":cancel,"row":5,"col":2},
    ])
# update function
def alert_update():
    # Alert
    alert_window=tk.Toplevel()
    alert_window.grab_set()
    label=ttk.Label(alert_window,text="Enter le Numero du Pilote : ")
    label.grid(column=1,row=1)
    enry=ttk.Entry(alert_window,)
    enry.grid(column=2,row=1)
    def update():
        pil=pilote1.recherche_par_num(int(enry.get()))
        alert_window.destroy()
        update_pilote(pil)
    btn_update=ttk.Button(alert_window,text="Update",command=update)
    btn_update.grid(column=2,row=3)

def update_pilote(pil):
    update_window=tk.Toplevel()
    update_window.grab_set()
    label_num=ttk.Label(update_window,text="Numero:")
    label_nom=ttk.Label(update_window,text="Nom:")
    label_ville=ttk.Label(update_window,text="Ville:")
    label_salaire=ttk.Label(update_window,text="Salaire:")
    # Entries
    num_entery=ttk.Entry(update_window)
    num_entery.insert(2,pil["num"])
    nom_entery=ttk.Entry(update_window)
    nom_entery.insert(2,pil["nom"])
    ville_entery=ttk.Entry(update_window)
    ville_entery.insert(2,pil["ville"])
    salaire_entery=ttk.Entry(update_window)
    salaire_entery.insert(2,pil["salaire"])
    def update():
        # update pilote
        if num_entery.get()!="" and nom_entery.get()!="" and ville_entery.get()!=""\
                            and salaire_entery.get()!="":
            pilote1.num=num_entery.get()
            pilote1.nom=nom_entery.get()
            pilote1.ville=ville_entery.get()
            pilote1.salaire=salaire_entery.get()
            pilote1.ajouterPilotes()
            update_window.destroy()
        else:
            messagebox.askretrycancel(message="vuillez remplir tous les champs")
    # buttons
    add=ttk.Button(update_window,text="Update",command=update)
    cancel=ttk.Button(update_window,text="Close",command=update_window.destroy)
    position(
    [
    {"obj":label_num,"row":1,"col":1},
    {"obj":num_entery,"row":1,"col":2},
    {"obj":add_btn,"row":1,"col":3},
    {"obj":label_nom,"row":2,"col":1},
    {"obj":nom_entery,"row":2,"col":2},
    {"obj":label_ville,"row":3,"col":1},
    {"obj":ville_entery,"row":3,"col":2},
    {"obj":label_salaire,"row":4,"col":1},
    {"obj":salaire_entery,"row":4,"col":2},
    {"obj":add,"row":5,"col":3},
    {"obj":cancel,"row":5,"col":2},
    ])
# root window
root=tk.Tk()
num=tk.StringVar(value="id")
nom=tk.StringVar(value="nom")
ville=tk.StringVar(value="ville")
salaire=tk.StringVar(value="salaire")
num=tk.Entry(root,textvariable=num,state=tk.DISABLED,font=('Arial',16,'bold'))
num.grid(row=1,column=0)
nom=tk.Entry(root,textvariable=nom,state=tk.DISABLED,font=('Arial',16,'bold'))
nom.grid(row=1,column=1)
ville=tk.Entry(root,textvariable=ville,state=tk.DISABLED,font=('Arial',16,'bold'))
ville.grid(row=1,column=2)
salaire=tk.Entry(root,textvariable=salaire,state=tk.DISABLED,font=('Arial',16,'bold'))
salaire.grid(row=1,column=3)
for i in range(len(pilote1.liste_pilotes())):
    for j in range(len(pilote1.liste_pilotes()[i].values())):
        mstr=tk.StringVar(value=list(pilote1.liste_pilotes()[i].values())[j])
        td=tk.Entry(root,textvariable=mstr,state=tk.DISABLED,font=('Arial',16,'bold'))
        td.grid(row=i+2,column=j,ipady=10)
# buttons
# add button
add_btn=ttk.Button(root,text="Add",command=add_pilote)
add_btn.grid(row=len(pilote1.liste_pilotes())+3,column=1,pady=10)
# update button
update_btn=ttk.Button(root,text="Update",command=alert_update)
update_btn.grid(row=len(pilote1.liste_pilotes())+3,column=2,pady=10)
#  delete button
ttk.Style().configure('W.TButton', font =
               ('calibri', 10, 'bold', 'underline'),
                foreground = 'red')
delete_btn=ttk.Button(root,text="Delete",style="W.TButton",command=delete_pilote)
delete_btn.grid(row=len(pilote1.liste_pilotes())+3,column=3,pady=10)
# reset_btn=ttk.Button(root,text="reset")
# reset_btn.grid(row=len(pilote1.liste_pilotes())+3,column=3)



root.mainloop()