-- Utilisation de l'instruction Case sous Mysql (équivalent de switch )
-- syntaxe :
   /*
   case 
    when condition1 then bloc instruction1
    when condition2 then bloc instruction2
    when condition3 then bloc instruction3
    ...............................
    else
       bloc instruction2
    end case;
    */
-- Ecrire une fonction qui retourne Etat du solde d'un client selon 
-- les situations suivantes : 
--  (Debiteur si solde negatif,créditeur si solde positf, neutre si solde =0)
-- créer une base de données de travail
-- create database programmingDev202;
-- use programmingDev202;
drop function if exists F_EtatSolde;
delimiter $$
create function F_EtatSolde(solde float) returns varchar(40)
deterministic
begin
     declare etat varchar(40);
      if solde = 0 then
         set etat :='neutre';
      elseif solde >0 then
              set etat ='créditeur';
      else
           set etat ='débiteur'; 
      end if;
      return etat;
end$$

-- test de la fonction F_EtatSolde
set @solde=-0;
select F_EtatSolde(@solde);
-- 2éme Solution 
drop function if exists F_EtatSolde_Bis;
delimiter $$
create function F_EtatSolde_Bis(solde float) returns varchar(40)
deterministic
begin
     declare etat varchar(40);
      case 
	  when solde = 0 then  set etat :='neutre';
      when solde >0 then   set etat ='créditeur';
      else      set etat ='débiteur'; 
      end case;
      return etat;
end$$
-- Test de la fonction F_EtatSolde_Bis
set @montant =7000;
select F_EtatSolde_Bis(@montant);

-- Test avec Case sans créer une fonction
drop procedure if exists P;
delimiter $$
create procedure P(in x float)
begin
select     
           case
              when x=0 then  'neutre'
              when x>0 then  'Créditeur'
              else      'Débiteur'
           end as 'Etat solde';
         
end$$

call P(0);
-- Exercice : Ecrire une fonction qui retourne  la parité d'un nombre ù
--  en utilisant l'instruction Case si nombre vaut 5 la fonction retourne
-- le message '5 est impair'
-- ***********************************************************
-- Exercice : Ecrire une fonction qui retourne  la mention de la moyenne
 -- d'un Etudaint en utilisant l'instruction Case 
 -- si moyenne >=0 et <10  la fonction retourne 'Echec'
  -- si moyenne >=10 et <12   la fonction retourne 'Passable'
  -- si moyenne >=12 et <14   la fonction retourne 'A.Bien'
  -- si moyenne >=14 et <16   la fonction retourne 'Bien'
  -- si moyenne >=16 et <18   la fonction retourne 'T.Bien'
  -- si moyenne >=18 et <=20   la fonction retourne 'Excellent'
  -- si moyenne est différente de ces valeurs la fonction retourne 'Erreur'




