-- créer une procedure stockée 
-- qui affiche la liste des livres
delimiter $$
create procedure PSListeLivre()
begin
        select * from livre;
end$$
-- appel de la procedure stockée
call PSListeLivre();
-- supprimer une procedure stockée
drop procedure PSlisteLivre;
-- Créer une procedure stockée qui affiche 
-- la liste des livres d'un auteur via son nom de famille
delimiter **
create procedure PSLivreAuteur( in x varchar(30))
begin
select L.* from livre L inner join auteur A
              on L.numaut=A.numaut
where  nom=x;
end **

-- Appel de la procedure PSLivreAuteur
call PSLivreAuteur('Nizar kabani');
-- créer une procedure stockée qui demande un N° auteur puis retourne
-- le nombre de livres ecrits par cet auteur
delimiter **
 create procedure PSNbreLivreByAuteur(in num int,out nbrelivre int)
begin
select count(refliv) into nbrelivre from livre
 where numaut=num;
end **

-- appel de cette procedure
set @num =1;
set @nbre=0;
call PSNbreLivreByAuteur(@num,@nbre);
select @nbre;
   







