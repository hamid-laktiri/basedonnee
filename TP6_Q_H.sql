-- Créer la procédure stockée qui Affiche les noms des étudiants 
-- classées les premiers (ayant la plus grande moyenne) ?
create temporary table Tmoy(code varchar(10) ,nom varchar(30) , moyenne float);
insert into Tmoy 
      (select e.Code_etudiant,e.Nom,avg(n.Note_obtenue) 
      from etudiant e inner join note  n 
               on e.Code_etudiant = n.Code_etudiant
      group by e.Code_etudiant,e.Nom);

delimiter $$
create procedure P_PremierEtudiant()
begin
declare maxnote float;
select max(moyenne) into maxnote from  Tmoy;
select code,nom
from Tmoy
where moyenne=maxnote;
end $$