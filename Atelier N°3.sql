-- consultation des données (affichages des enregistrements)
/*
    select  liste_des_colonnes [*] 
    from nomtable1 ,nomtable2,....
    [where  condition]
*/
-- afficher les livres
select * from livre;
/*
fonctions logiques   and ,or 
fonctions de comparaisons : = > < >= <= <> != between like
opération arithmétiques : + - / * mod 
*/
-- afficher les livres de prix >=150
select * from livre where prix>=150;
-- afficher les livres ayant plus de 200 pages 
-- et le prix est entre 100 et 180
select * from livre 
where (prix between 100 and 180) and nbre_page>200;
-- afficher les livres et le nom de l'auteur 
-- pour les auteurs français
select L.* ,A.nom
from livre L inner join auteur A on L.numaut=A.numaut
where A.pays='maroc';
/*--------------------*/
select L.* ,A.nom
from livre L right join auteur A on L.numaut=A.numaut
where A.pays='maroc';
/*--------------------*/
select L.* ,A.nom
from livre L left join auteur A on L.numaut=A.numaut
where A.pays='maroc';
-- afficher refliv,nom auteur, titre ,prix tels que 
-- nom auteur commence par 'f' et se termine ar 'i' 
-- nbre_page  superieur ou égal à 250

select l.refliv,a.nom,L.titre,l.nbre_page,a.pays
from livre  l ,auteur a
where l.numaut=a.numaut
  and a.nom like 'f%i'
   and l.nbre_page>=250;
/*--------------------------------*/
select l.refliv,a.nom,L.titre,l.nbre_page,a.pays
from livre  l inner join auteur a 
               on l.numaut=a.numaut
where  a.nom like 'f%i'
   and l.nbre_page>=250;
-- les fonctions d'agrégation
-- sum ,avg ,count,min ,max
-- aficher le plus petit,le plus grand prix
-- la moyenne des prix et le nombre de livre
select min(prix) as 'Min-prix',max(prix) as 'Max_prix',
    avg(prix) as 'Moy_prix',count(refliv) as 'nbre_libre'
from livre;
-- afficher les pays des auteurs
select distinct  pays
from auteur;
-- afficher la somme des prix des livres sans remises
-- et avec remise
select sum(prix) ,sum(prix*(1-remise/100))
from livre ;
-- le regroupement ou partitionnement : 
-- on utilise le mot group by sur le champs de regroupement
-- affciher pour chaque pays le nombre de livres écrits
select  auteur.pays,count(livre.refliv)
from auteur inner join livre 
      on livre.numaut=auteur.numaut
group by auteur.pays;
-- afficher les pays ayant  2 livres écrits ou plus
select  auteur.pays
from auteur inner join livre 
      on livre.numaut=auteur.numaut
group by auteur.pays
having count(livre.refliv)>=2;
-- Pour effectuer un tri sur une colonne
-- on utilise le mot order by nom_colonne
-- exemple afficher les livres tries par titre croissant 
-- puis par prix decroissnant
select * from livre
order by titre desc , prix desc;
-- le mot union ,intersect , except

select 2 as 'val1', 4 as 'val2'
union 
select 8 as 'val1', 9 as 'val2';

-- afficher les villes de naissances des formateurs 
-- et celles des stagiaires
select distinct villenatale as 'ville'
from formateur
union
select distinct ville_naissance as 'ville'
from stagiaire;
-- afficher les villes de naissances des formateurs figurant dans 
-- la liste des villes natale des stagiaires
-- Equivalent de intersect
select distinct villenatale as 'ville'
from formateur
where villenatale in ( select distinct ville_naissanc
                       from stagiaire);
-- afficher les villes de naissances des formateurs  qui ne 
-- figurant  pas dans la liste des villes natale des stagiaires
-- Equivalent de Except 
select distinct villenatale as 'ville'
from formateur
where villenatale not in ( select distinct ville_naissanc
                       from stagiaire);








