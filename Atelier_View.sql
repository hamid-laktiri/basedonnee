-- Céer   une view qui affiche le nombre
-- de livre écrits par chaque auteur(num_aut,nom_pays)
use db01;

create view V1 (code_auteur,nom_auteur,pays_auteur,nombre_livre)
 as
select  auteur.numaut,auteur.nom,auteur.pays,
count(livre.refliv) as 'nbre_livre'
from auteur left join livre on auteur.numaut=livre.numaut
group by auteur.numaut,auteur.nom,auteur.pays;
-- afficher le résultat de cette vue
select * from V1;
-- afficher les auteurs ayant plus ou égal 
-- à deux livres
select * from V1 where nombre_livre>=2;
-- afficher les auteurs n'ayant pas encore déposé leur  livre
select V1.code_auteur,nom_auteur,pays_auteur
from V1 where nombre_livre=0;
select * from auteur limit 3 offset 0;


