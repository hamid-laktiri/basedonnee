-- se connecter à la base de données
use BDCommerce;
-- lancer le script sql
CREATE TABLE clients (
id INT NOT NULL AUTO_INCREMENT,
nom VARCHAR(50) NOT NULL,
prenom VARCHAR(40),
date_naissance DATE,
adresse VARCHAR(50) not null  DEFAULT 'Agadir',
PRIMARY KEY (id)
);
/*-- -------------------------------- */
use bdcommerce;
create table commande
( refcmd varchar(10) not null ,
  numclt int  not null  ,
  datecmd date not null  , 
   primary key (refcmd),
  foreign key (numclt) references clients (id)  
);
-- Ajouter la colonne email dans la table clients
use bdcommerce;
alter table clients add email varchar(30)
      not null default 'id.nom@gmail.com';
-- supprimer la colonne email de la table clients
alter table clients drop column email;
-- modifier la taille de la colonne email 
alter table clients modify column email varchar(50)  
          not null default 'id.nom@gmail.com';
-- ajouter la colonne sexe aprés le prenom et pays aprés datenaissance
alter table clients add sexe varchar(1) not null default 'm'
               check (sexe in ('m','f')) after prenom,
               add pays varchar(40) default 'maroc' after date_naissance;
/* Ajouter une contrainte qui n'accepte que la date commande
     >= à aujoud'hui*/
   alter table commande add constraint cst_datecmd
      check (
                 datediff(datecmd ,date(now())) >= 0
              ) ;
 
 -- créer une table simulaire à la table commande
 
 create table commande2 like commande;
 create table clients2 like clients;
 
-- afficher la liste des tables
  show tables;
-- afficher la description d'une table
describe  commande;
-- créer la table article
create table article (
          numart int not null  -- numart>0
          ,libelle varchar(100) not null,
          prix float -- >0 
          ,seuil int not null default 100,
          qtitesotck int not null   -- >seuil
                  );
-- ajouter les constraintes (primary key et les condistion sur les colonnes
alter table article add constraint cst_pk primary key(numart);
alter table article add constraint cst_numart check (numart >0);
alter table article add constraint cst_prix check (prix >0);
alter table article add constraint cst_seuil check (seuil >0);
alter table article add constraint cst_qtitestock 
        check (qtitesotck > seuil);




