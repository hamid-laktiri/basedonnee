-- I. Instructions Declare (declaration d'une variable)
-- et set(affectation d'une valeur à la variable

-- syntaxe pour la déclaration: declare nomvar type;
-- syntaxe pour l'affectation d'u ne valeur: set nomvar :=valeur;
/*
delimiter $$
drop procedure if exists P1;
create procedure P1()
begin
declare x int; -- la déclaration
set x=3;   -- c'est l'affectaion
select x;
end$$
-- -------------Le test du programme --------------------
call P1();
*/
-- Ecrire un script sql qui affiche la somme de 2 valeurs
-- Solution
/*
drop procedure if exists P2;
delimiter $$
create procedure P2()
begin
declare x int;
declare  y int;
declare z int;
set x=5; set y=8;
set z=x+y;
select x,'+',y,'=',z;
end$$
-- -------------Le test du programme --------------------
call P2();
*/
-- --    ou bien------------------
set @a :=30;     -- déclare et initialise la variable a
set @b =50;      -- déclare et initialise la variable b
select @a,'+',@b,'=',(@a+@b);
-- ou bien on peut écrire comme ceci :

select 5 into @w;  -- set @w=5;
set @w=7,@t=6,@z=(@w+@t);
select @w,'+',@t,'=',@z,(@w+@t);

-- II.notion de structure alternative
-- syntaxe 
/*
   if condition then
          bloc d'instruction 1;
    else
           bloc d'instruction 2
   end if;
*/
-- Exemple  afficher le signe d'un nombre (positif ou négatif)
-- par défaut la valeur zéro est positive

drop procedure if exists P3;
delimiter $$
create procedure P3(in k float,out reponse varchar(40))
begin
  if k>=0 then
      
      select (concat(k, ' is positif') ) into reponse;
      
  else   
  
        select (concat(k, ' is négatif') ) into reponse;
  
  end if;  
end$$

-- Exécuter la procedure stockée P3
set @rep=' ';
call P3(4,@rep);  select @rep;
call P3(-4,@rep);  select @rep;

-- Exemple  afficher le signe d'un nombre (positif ou négatif avec zero est neutre)
drop procedure if exists P4;
delimiter $$
create procedure P4(in k float,out reponse varchar(40))
begin
  if k>0 then
      
      select (concat(k, ' is positif') ) into reponse;
      
  elseif k<0 then   
  
        select (concat(k, ' is négatif') ) into reponse;
  else
      select (concat(k, ' est neutre') ) into reponse;
  end if; 
     -- select @reponse;
end$$
-- Execution de P4
set @retour = '';
call P4(-9,@retour);
select @retour;
 

-- III. Notion de structure Itérative

-- syntaxe   
        /*
        nombloc: loop

				leave nombloc;
                
                iterate nombloc;
                
             end loop
        */

-- Ecrire un programme Mysql qui affiche les nombres entre 1 et 5
-- Solution
   drop procedure if exists P5;
   delimiter $$
   create procedure P5()
   begin
     declare i int;
     create temporary table  T (col int);
     set i=1;
           bloc:loop
           if i>5 then
             leave bloc;
           end if;
           insert into T select i ;
           set i=i+1;
           -- iterate bloc;
           end loop bloc;
   end$$
-- Exécution de P5
drop Temporary Table if Exists T;
call P5();
select * from T;
-- Deuxième Solution avec la notion While
-- Styntaxe :
/*
      while condition do
          bloc instruction;
      end while;
*/
drop procedure if exists P6;
delimiter $$
create procedure P6()
begin
declare k int;
set k=1;
create temporary Table R(val int);
     while k<=5 do
       insert into R values(k);
       set k =k+1;
     end while;
end$$
-- Le test de la procédure P6
drop temporary Table if exists R;
call P6();
select * from R;

