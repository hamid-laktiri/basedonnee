-- Ecrire une procedure sstockée qui retourne le nombre de client
-- masculin et le nombre" de client feminin.
use  db_fetch;
drop procedure if exists P2_Fetch;
delimiter $$
create procedure P2_Fetch(out n1 int,out n2 int,out n int)
begin
       declare sexe varchar(1);
       declare fin_loop int;
       declare curs2 cursor for select sexeclt from Tclient;
       declare continue handler for not found set fin_loop=1;
       set n1=0;     -- nbre de client masculin initialisé à 0
       set n2=0;     -- nbre de client féminin initialisé à 0
       set n =0; -- nbre de client
       open curs2;
         fetch curs2 into sexe;
         x:loop
              if fin_loop = 1 then
                 leave x;
              end if;
              if sexe= 'm' then
                     set n1 =n1+1;
              else
                    set n2=n2+1;
              end if;
              set n=n+1;
           fetch curs2 into sexe;
         end loop ;
       close curs2;
end $$
-- Test P2_Fetch
set @x =0;
set @y=0;
set @z=0;
call db_fetch.P2_Fetch(@x,@y,@z);
select @x as 'nombre de client Masculin',
      @y  as 'nombre de client féminin';
select @x/@z as '% de client Masculin',
      @y/@z  as '% de client féminin';      
-- ********************************************************
-- Exemple : ecrire une procedure stockée qui remplit 
-- une table temporaire Nombre avec des valeurs 
-- comprises entre 1 et 100; puis affiche le nombre 
-- de multiple de 4 et celui de 5;
-- ****************************************************
drop temporary  table if exists Tnbre;
create temporary table Tnbre(nbre int);
drop procedure if exists Charger_Tnbre;
delimiter $$
create procedure Charger_Tnbre()
begin
declare i int;
 set i=1;
       while i<=100 do
          insert into Tnbre values(i);
          set i=i+1;
       end while;
end $$       
delimiter ; 
call Charger_Tnbre();
drop procedure if exists P3_Fetch;
delimiter $$
create procedure P3_Fetch(out varmultiple4 int,out varmultiple5 int )
begin
  
    declare z int;
    declare v1 int;
    declare v2 int;
    declare point_arret int;
    declare curs3 cursor for select * from Tnbre;
    declare continue handler for not found set point_arret=1;
    set v1=0;
    set V2=0;
   open curs3;
     fetch curs3 into z;
        bloc:loop
               if point_arret=1 then
                  leave bloc;
               end if;
               if z % 4 = 0 then
                 set v1 = v1 +1;
                 
               end if;
               if z % 5 = 0 then
                 set v2 = v2 + 1;
                
               end if;
               fetch curs3 into z;
        end loop;
        select  v1 into varmultiple4;
        select v2 into varmultiple5;
   close curs3;
end $$


-- ********************************
set @a =0;
set @b =0;
call P3_fetch(@a,@b);
select @a as 'nbre de multiple de 4 entre 1 et 100',
       @b as 'nbre de multiple de 5 entre 1 et 100';








