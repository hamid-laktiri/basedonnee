Use Parking;
select count(salaire) from conducteur
where salaire> 3500;
-- Q1
select avg(salaire) from conducteur;
-- Q2
select max(salaire) as plusgrandsalaire , min(salaire) as plusfaiblesalaire
from conducteur;
-- Q3
select sum(salaire) as AllAgadirSalaires 
from conducteur
where ville = 'Agadir';
-- Q4

select * from conducteur 
inner join service on conducteur.code_service = service.code_service
where service.nom_service = 'Comptabilite' Or service.nom_service='entretien';
-- Q5
select matricule, Puissance, Type from voiture
inner join marque on marque.code_marque = marque.code_marque
where marque.libelle = 'Toyota' Or marque.libelle='Fiat';
-- Q6
select nom_service from service 
right join conducteur on conducteur.code_service = service.code_service
where conducteur.salaire >2000 and conducteur.salaire <3000;
-- Q7
select * from voiture 
inner join conducteur on conducteur.code_service = voiture.code_service
where conducteur.ville = 'Agadir';
-- Q8
select matricule , Puissance, type from voiture
inner join marque on marque.code_marque = voiture.code_marque
inner join conducteur on conducteur.code_service = voiture.code_service
where marque.libelle = 'Fiat' and conducteur.nom = 'Mouhssine';
-- Q9
select * from voiture
inner join conducteur on conducteur.code_service = voiture.code_service
where conducteur.ville Not Like'Rabat' and conducteur.ville Not Like 'Agadir';
-- Q10
select marque.libelle from marque
inner join voiture on marque.code_marque = voiture.code_marque
inner join service on service.code_service = voiture.code_service
where nom_service = 'comptabilite' ;
-- Q11
select code, nom, salaire from conducteur
inner join service on service.code_service = conducteur.code_service
where nom_service Not Like 'Vente' and ville = 'rabat' ;
-- Q12
select * from voiture
inner join service on service.code_service = voiture.code_service
where nom_service Not Like 'Vente' and nom_service Not Like 'comptabilite';
-- Q13
select matricule, puissance, type from voiture
inner join marque on marque.code_marque = voiture.code_marque
inner join conducteur on voiture.code_service = conducteur.code_service
where libelle = 'Fiat' and nom Like 'M%e';
-- Q15
select * from voiture 
inner join service on voiture.code_service = service.code_service
where (nom_service like 'Comptabilite' or nom_service like 'Vente') and nom_service not like 'Peugeot';
-- Q16
select code, nom_service,salaire,ville from conducteur 
inner join service on conducteur.code_service = service.code_service
where salaire > 3500 or ville = 'Casablanca';
-- Q17
select Matricule, Puissance, type , libelle, nom_service from voiture
inner join service on voiture.code_service = service.code_service
inner join marque on marque.code_marque = voiture.code_marque
where type = 'Gazoil';