Use Parking;
-- Q1
select  ville, avg(salaire), sum(salaire) from conducteur
group by ville;
-- Q2
select libelle, count(Matricule) from marque
right join voiture on voiture.code_marque = marque.code_marque
group by libelle;
-- Q3
select nom_service, count(matricule) from voiture
left join service on service.code_service=voiture.code_service
group by nom_service;
-- Q4
select nom_service, count(code_marque) from voiture
inner join service on voiture.code_service = service.code_service 
group by nom_service;
-- Q5
select nom_service, salaire m from service
inner join conducteur on conducteur.code_service = service.code_service
where salaire > 3000 and salaire < 5000;
-- Q6
select nom_service, max(salaire), min(salaire), avg(salaire) from conducteur
inner join service on conducteur.code_service = service.code_service
group by nom_service;
-- Q7
select nom_service, count(matricule), libelle from voiture
inner join service on service.code_service = voiture.code_service
inner join  marque on marque.code_marque = voiture.code_marque
group by nom_service;
-- Q8
select nom_service, count(code) from conducteur
inner join service on service.code_service = conducteur.code_service
group by nom_service
having count(code)> 3;
-- Q9
select avg(salaire), ville from conducteur
group by ville
having avg(salaire) <=6500;
-- Q10

select nom_service, nom from conducteur
inner join service on service.code_service = conducteur.code_service
group by nom_service
order by salaire DESC;