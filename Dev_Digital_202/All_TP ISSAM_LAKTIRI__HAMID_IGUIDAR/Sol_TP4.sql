create database BD_LPSIL;
Use BD_LPSIL;
create table Filiere (
Code_Filiere varchar(8),
Nom_filiere varchar(25),
primary key (Code_Filiere));

create table etudiant (
Code_Etudiant varchar(8),
Code_Filiere varchar(8),
Nom varchar(15),
ville varchar(15),
Age integer,
primary key (Code_Etudiant),
foreign key (Code_Filiere) references Filiere(Code_Filiere));

create table Module (
Code_Module varchar(8),
Code_Filiere varchar(8),
Libelle_Module varchar(25),
primary key (Code_Module),
foreign key (Code_Filiere) references Filiere(Code_Filiere));

create table Note (
Code_Etudiant varchar(8),
Code_Module varchar(8),
Note_obtenue double,
foreign key (Code_Module) references Module(Code_Module),
foreign key (Code_Etudiant) references Etudiant(Code_Etudiant));

insert into Filiere(code_Filiere,Nom_filiere)
values ('F001', 'Informatique de gestion 1');
insert into Filiere(code_Filiere,Nom_filiere)
values ('F002', 'Informatique de gestion 2');
insert into Filiere(code_Filiere,Nom_filiere)
values ('F003', 'webmaster1');
insert into Filiere(code_Filiere,Nom_filiere)
values ('F004', 'Administrateur Reseau1');

insert into Module(code_module, code_Filiere,Libelle_module)
values ('M001', 'F001', 'Bureautique');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M002', 'F001', 'Algorithme');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M003', 'F001', 'Comptabilite');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M004', 'F002', 'visual basic');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M005', 'F002', 'Sql server');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M006', 'F002', 'language C');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M007', 'F003', 'Infographie');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M008', 'F003', 'frontpage2000');
insert into Module(code_module, code_Filiere,Libelle_module)
values ('M009', 'F004', 'Les Reseaux');

insert into Conducteur(code,code_service, Nom, Ville, Salaire)
values ('cond001', 'serv001', 'salim', 'Agadir', 27000);
insert into Conducteur(code,code_service, Nom, Ville, Salaire)
values ('cond002', 'serv002', 'Mouhssine', 'Casablanca', 3500);
insert into Conducteur(code,code_service, Nom, Ville, Salaire)
values ('cond003', 'serv003', 'Yakoubi', 'Agadir', 6000);
insert into Conducteur(code,code_service, Nom, Ville, Salaire)
values ('cond004', 'serv004', 'kasbi', 'Rabat', 2800);

SELECT * FROM Etudiant INNER JOIN Filiere ON Filiere.code_filiere = Etudiant.code_filiere WHERE Filiere.nom_filiere = "Webmaster1";
-- Q1
SELECT * FROM Etudiant WHERE ville = "Agadir" OR ville = "Rabat";
-- Q2
SELECT * FROM Module INNER JOIN Filiere ON Filiere.code_filiere = Module.code_filiere WHERE Filiere.nom_filiere = "Administrateur Réseau1";
-- Q3
SELECT COUNT(code_module) FROM Module GROUP BY code_filiere;
-- Q4
SELECT * FROM Etudiant INNER JOIN Note ON Etudiant.code_etudiant = Note.code_etudiant GROUP BY Note.code_etudiant Having Note.note_obtenue < 10;
-- Q5
SELECT AVG(note_obtenue) FROM Note GROUP BY code_module;
-- Q6
SELECT Etudiant.nom,AVG(note_obtenue) FROM Note INNER JOIN Etudiant ON Etudiant.code_etudiant = Note.code_etudiant GROUP BY Note.code_etudiant;
-- Q7
SELECT Etudiant.code_filiere,Etudiant.nom,AVG(note_obtenue) FROM Note INNER JOIN Etudiant ON Etudiant.code_etudiant = Note.code_etudiant GROUP BY Note.code_etudiant Having Etudiant.code_filiere = (SELECT code_filiere FROM Filiere WHERE nom_filiere = "Webmaster1");
-- Q8
SELECT nom FROM Etudiant WHERE ville = "Casablanca" AND age BETWEEN 18 AND 20;