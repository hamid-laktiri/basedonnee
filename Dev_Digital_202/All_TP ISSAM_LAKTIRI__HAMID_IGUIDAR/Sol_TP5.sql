create database AviationA;
use AviationA;
create table PILOTE(
NUMPIL int primary key not null,
PILNOM varchar(10),
ADR varchar(15)
);
create table AVION(
NUMAV int primary key not null,
AVMARQ varchar(12),
AVTYPE varchar(10),
CAP int,
LOC varchar(15)
);
create table VOL(
NUMVOL varchar(6),
NUMAV int,
NUMPIL int,
VD varchar(15),
VA varchar(10),
HD int,
HA int
);
alter table PILOTE add salaire int;
alter table vol add primary key (numvol);
alter table vol add foreign key(numpil) references pilote(numpil);
alter table vol add foreign key(NUMAV) references AVION(NUMAV);
insert into avion value 
("100","AIRBUS","A320","300" , "Nice"),
("101","BOIENG","B707","250" , "Paris"),
("102","AIRBUS","A320","300" , "Toulouse"),
("103","CARAVELLE"," Caravelle","200" , "Toulouse"),
("104","BOEING","B747","400" , "Paris"),
("105","AIRBUS","A320","300" , "Grenoble"),
("106","ATR","ATR42","50" , "Paris"),
("107","BOEING","B727","300" , "Lyon"),
("108","BOEING","B727","300" , "Nantes"),
("109","AIRBUS","A320","350" , "Bastia")
;

insert into pilote value 
('1', 'SERGE', 'Nice',"7500"),
('2' ,'JEAN' ,'Paris',"8500"),
('3', 'CLAUDE', 'Grenoble',"7500"),
('4' ,'ROBERT' ,'Nantes',"25000"),
('5', 'MICHEL', 'Paris',"10500"),
('6' ,'LUCIEN' ,'Toulouse',"19000"),
('7', 'BERTRAND', 'Lyon',"15200"),
('8' ,'HERVE' ,'Bastia',"9500"),
('9', 'LUC', 'Paris',"17500");


-- update pilote set salaire=7500 where NUMPIL=1;
-- update pilote set salaire=8500 where NUMPIL=2;
-- update pilote set salaire=25000 where NUMPIL=3;
-- update pilote set salaire=10500 where NUMPIL=4;
-- update pilote set salaire=19000 where NUMPIL=5;
-- update pilote set salaire=15200 where NUMPIL=6;
-- update pilote set salaire=9500 where NUMPIL=7;
-- update pilote set salaire=7500 where NUMPIL=8;
-- update pilote set salaire=17500 where NUMPIL=9;

insert into vol value 
("IT100","100"," 1","NICE" , "PARIS","7"," 9"),
("IT101","100"," 2","PARIS" , "TOULOUSE","11"," 9"),
("IT102","101"," 1","PARIS" , "NICE","7"," 12"),
("IT103","105"," 3","GRENOBLE" , "TOULOUSE","9"," 9"),
("IT104","105"," 3","TOULOUSE" , "GRENOBLE","17"," 9"),
("IT105","107"," 7","LYON" , "PARIS","7"," 6"),
("IT106","109"," 8","BASTIA" , "PARIS","7"," 10"),
("IT107","106"," 9","PARIS" , "BRIVE","7"," 7"),
("IT108","106"," 9","BRIVE" , "PARIS","7"," 19"),
("IT109","107"," 7","PARIS" , "LYON","7"," 18"),
("IT110","102"," 2","TOULOUSE" , "PARIS","7"," 15"),
("IT111","101"," 4","NICE" , "NANTES","7"," 17")
;
-- Q1
CREATE VIEW V1 AS SELECT numpil,pilnom FROM Pilote WHERE salaire BETWEEN 7500 AND 8500;
-- Q2
CREATE VIEW V2 AS SELECT pilnom FROM Pilote WHERE pilnom LIKE "s%e";
-- Q3
CREATE VIEW V3 AS SELECT pilnom FROM Pilote WHERE pilnom LIKE "_s%";
-- Q4

CREATE VIEW V4 AS SELECT * FROM Vol WHERE ha = 19;
-- Q5

CREATE VIEW V5 AS SELECT * FROM Vol WHERE vd = "Paris";
-- Q6
CREATE VIEW V6 AS SELECT avtype FROM Avion WHERE cap > 200;
-- Q7
CREATE VIEW V7 AS SELECT count(DISTINCT numav) FROM Vol;
-- Q8
CREATE VIEW V8 AS SELECT * FROM Avion WHERE cap = (SELECT min(cap) FROM Avion);
-- Q9
CREATE VIEW V9 AS SELECT MIN(cap),MAX(cap) FROM Avion WHERE avmarq = "Boeing";
-- Q11
CREATE VIEW V10 AS SELECT AVG(cap) FROM Avion WHERE loc = "Paris";
-- Q12
CREATE VIEW V11 AS SELECT SUM(cap) FROM Avion;
-- Q13
CREATE VIEW V12 AS SELECT * FROM Vol INNER JOIN Avion ON Avion.numav = Vol.numav WHERE Vol.va="Paris" AND Avion.avmarq = "AIRBUS";
-- Q14
CREATE VIEW V13 AS SELECT * FROM Vol INNER JOIN Avion ON Avion.numav = Vol.numav WHERE Vol.va != "Paris" AND Avion.avmarq = "AIRBUS";

-- Q15
CREATE VIEW V14 AS SELECT * FROM Avion WHERE cap > 200 OR avmarq = "AIRBUS";
-- Q16
CREATE VIEW V15 AS SELECT Pilote.pilnom FROM Vol INNER JOIN Pilote ON Pilote.numpil = Vol.numpil WHERE Vol.vd = "Paris";





