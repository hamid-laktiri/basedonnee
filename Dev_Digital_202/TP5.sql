create database AviationA;
use AviationA;
create table PILOTE(
NUMPIL int primary key not null,
PILNOM varchar(10),
ADR varchar(15)
);
create table AVION(
NUMAV int primary key not null,
AVMARQ varchar(12),
AVTYPE varchar(10),
CAP int,
LOC varchar(15)
);
create table VOL(
NUMVOL varchar(6),
NUMAV int,
NUMPIL int,
VD varchar(15),
VA varchar(10),
HD int,
HA int
);
alter table PILOTE add salaire int;
alter table vol add primary key (numvol);
alter table vol add foreign key(numpil) references pilote(numpil);
alter table vol add foreign key(NUMAV) references AVION(NUMAV);
insert into avion value 
("100","AIRBUS","A320","300" , "Nice"),
("101","BOIENG","B707","250" , "Paris"),
("102","AIRBUS","A320","300" , "Toulouse"),
("103","CARAVELLE"," Caravelle","200" , "Toulouse"),
("104","BOEING","B747","400" , "Paris"),
("105","AIRBUS","A320","300" , "Grenoble"),
("106","ATR","ATR42","50" , "Paris"),
("107","BOEING","B727","300" , "Lyon"),
("108","BOEING","B727","300" , "Nantes"),
("109","AIRBUS","A320","350" , "Bastia")
;

insert into pilote value 
('1', 'SERGE', 'Nice',"7500"),
('2' ,'JEAN' ,'Paris',"8500"),
('3', 'CLAUDE', 'Grenoble',"7500"),
('4' ,'ROBERT' ,'Nantes',"25000"),
('5', 'MICHEL', 'Paris',"10500"),
('6' ,'LUCIEN' ,'Toulouse',"19000"),
('7', 'BERTRAND', 'Lyon',"15200"),
('8' ,'HERVE' ,'Bastia',"9500"),
('9', 'LUC', 'Paris',"17500");

update pilote set salaire=7500 where NUMPIL=1;
update pilote set salaire=8500 where NUMPIL=2;
update pilote set salaire=25000 where NUMPIL=3;
update pilote set salaire=10500 where NUMPIL=4;
update pilote set salaire=19000 where NUMPIL=5;
update pilote set salaire=15200 where NUMPIL=6;
update pilote set salaire=9500 where NUMPIL=7;
update pilote set salaire=7500 where NUMPIL=8;
update pilote set salaire=17500 where NUMPIL=9;

insert into pilote value 
("IT100","100"," 1","NICE" , "PARIS","7"," 9"),
("IT100","100"," 1","NICE" , "PARIS","7"," 9")
:






