use AviationA;
create table avion(
numav int not null primary key ,
marque_avion int not null,
type_avion int not null ,
CAP int not null,
loc varchar(20) not null );
use AviationA;
create table pilote(
numpil int not null primary key,
pilnom varchar (20) not null ,
adresse varchar(50) not null,
salaire float not null
);
create table vol(
numvol int not null primary key,
numav int not null ,
numpil int not null,
ville_départ int not null ,
ville_arriv int not null ,
heure_départ int not null,
foreign key (numav) references avion(numav) on update cascade on delete cascade,
foreign key (numpil) references pilote(numpil) on update cascade on delete cascade

);
use AviationA;
insert into pilote  values 
('1','serge','nice') ,
 ('2','jean','paris'),
  ('3','CLAUDE ','Grenoble') ,
  ('4','ROBERT','Nantes'),
   ('5','MICHEL','Paris') ,
   ('6','LUCIEN','Toulouse'),
    ('7','BERTRAND','Lyon'),
     ('8','HERVE','Bastia'),
      ('9','LUC','Paris');



