create database BD_LPSIL;
Use BD_LPSIL;
create table Filiere (
Code_Filiere varchar(10) not null primary key,
Nom_filiere varchar(40) not null );

create table etudiant (
Code_Etudiant varchar(10) not null primary key ,
Code_Filiere varchar(10) not null,
Nom varchar(40) not null ,
ville varchar(40) not null,
Age integer not null ,
foreign key (Code_Filiere) references Filiere(Code_Filiere));

create table Module (
Code_Module varchar(10) not null primary key,
Code_Filiere varchar(10) not null ,
Libelle_Module varchar(40) not null,
foreign key (Code_Filiere) references Filiere(Code_Filiere));

create table Note (
Code_Etudiant varchar(10) not null ,
Code_Module varchar(10) not null ,
Note_obtenue decimal(18.9) not null ,
foreign key (Code_Module) references Module(Code_Module),
foreign key (Code_Etudiant) references etudiant(Code_Etudiant));

insert into Filiere values ('F001', 'Informatique de gestion 1'),
                           ('F002', 'Informatique de gestion 2'),
                           ('F003', 'webmaster1'),
                           ('F004', 'Administrateur Reseau1');

insert into Module values ('M001', 'F001', 'Bureautique'),
 ('M002', 'F001', 'Algorithme'),
 ('M003', 'F001', 'Comptabilite'),
 ('M004', 'F002', 'visual basic'),
 ('M005', 'F002', 'Sql server'),
 ('M006', 'F002', 'language C'),
 ('M007', 'F003', 'Infographie'),
 ('M008', 'F003', 'frontpage2000'),
 ('M009', 'F004', 'Les Reseaux');

insert into etudiant values ('etu001', 'f001', 'mouhacine', 'Agadir', 22),
('etu002', 'f001', 'gharib', 'casablanca', 23),
('etu005', 'f002', 'fadil', 'safi', 21),
('etu006', 'f002', 'taoussi', 'casablanca', 24),
('etu007', 'f003', 'allam', 'Agadir', 20),
('etu009', 'f003', 'soulami', 'rabat', 19),
('etu011', 'f004', 'slimani', 'Agadir', 18),
('etu012', 'f004', 'salami', 'rabat', 23);

insert into note values ('etu001','M001',15),
('etu002','M001',10),
('etu003','M001',9),
('etu001','M002',10),
('etu002','M002',8),
('etu003','M002',9),
('etu004','M004',8),
('etu005','M004',12),
('etu006','M004',15),
('etu004','M005',9),
('etu005','M005',12),
('etu006','M005',12); 

drop procedure if exists etud_par_ville ;
delimiter $$
create procedure etud_par_ville(ville varchar (40))
begin 
select * from etudiant 
where ville = 'agadir' and ville ='rabat';
end $$

drop procedure if exists module_par_filiere ;
delimiter $$
create procedure module_par_filiere(nom_filiere varchar (40))
begin 
select * from module
inner join filiere on module.code_filiere=filiere.code_filiere
order by nom_filiere;
end $$

etudiant_par_nomdrop procedure if exists etudiant_par_nom ;
delimiter $$
create procedure etudiant_par_nom(ville varchar (40))
begin 
select * from etudiant
 where nom like '%a%' or nom like '%r%';
end $$

drop procedure if exists nbr_module_par_filiere ;
delimiter $$
create procedure nbr_module_par_filiere( varchar (40))
begin 
select * from module
inner join filiere on module.code_filiere=filiere.code_filiere
order by nom_filiere;
end $$
