-- Les curseurs en Mysql (Fetch)
-- Définition:
/*
 curseur est un composant de mysql indispensable et essentiel
 dans les procedures stockées il permet de parcourir 
 les enregistrements et de manipuler les données pour acomplir
 une tâche.
 le curseur parcourt les lignes d'une tables de façon
 itérative du début jusqu'à la fin de la table. 
*/ 
use db_fetch;
drop table if exists Tclient;
create table Tclient (numclt int not null primary key,
                      nomclt varchar(40) not null,
                      sexeclt char(1) not null default 'm'
					    check(  sexeclt in ('m','f') )					
                       );
insert into Tclient values(1,'widdani','m'),
                          (2,'Fatihi','f'),
                          (4,'Zeroual','m'),
                          (12,'Salih','m');

select * from Tclient;
-- Utilisation des curseurs
-- 1ére étape : Déclarer le curseur
-- Syntaxe : 
          declare nom_curseur  cursor 
               for select * from Tclient;
-- 2éme étape : ouvrir le curseur
-- syntaxe : open nom_curseur
-- 3éme étape : la lecture du curseur 
    -- syntaxe : fetch nom_curseur into nom_variables
-- 4éme étape : fermeture du curseur
  -- syntaxe : close nom_curseur;
  -- *****************************************************************
  -- Remarque : pour savoir que le curseur a attient 
  -- la dernière ligne de la requete select on doit vérifier
  -- une variable poit arrêts si elle vaut 1 càd on'est à 
  -- la fin des lignes de la requete select sinon 
  -- il y'a encore d'autre à parcourir par l'instruction fetch.
  -- *****************************************************************
-- Exemple N°1 : Afficher les clients masculins en 
-- utilisant le curseur.
  -- Solution :
  drop procedure if exists P1_fetch;
  -- créer une table temporaire pour stocker les
  -- nom des clients masculins
  drop temporary table if exists Tem_client;
  create temporary table Tem_client(nom varchar(40));
  delimiter $$
  create procedure P1_fetch()
  begin 
  -- Déclaration des variables de travail
    declare num int;
    declare nom varchar(40);
    declare sexe varchar(1);
    -- Déclaration du poit arrêt
    declare point_arret int;
     -- déclaration du curseur curs1
    declare curs1 cursor for select sexeclt,nomclt,numclt from Tclient;
    declare continue handler for not 
           found set point_arret=1;
    -- ouverture du curseur curs1
    
    open curs1;
     fetch curs1 into sexe,nom,num;
  bloc: loop 
     if point_arret = 1  then
       leave bloc;
     end if ;     
     if sexe= 'm' then
      insert into Tem_client values(nom);
    end if;
    fetch curs1 into sexe,nom,num;
	end loop;
    -- Fermeture du curseur
    close curs1;
      select * from tem_client;
end$$
   -- appel de la prcedure stockée P1_fetch()
   call P1_fetch();
  
 
 
  
  
  