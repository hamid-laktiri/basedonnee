-------------------------------
------ TP Base de Donnée ------
----      SAID MAALOUM     ----
----         DEV202        ----
-------------------------------

-- Q1 
CREATE DATABASE IF NOT EXISTS parking;
USE parking;

-------------------------------

-- Q2
-- table service
CREATE TABLE service(
    code_service VARCHAR(10) NOT NULL PRIMARY KEY,
    nom_service VARCHAR(15)
);
-- table conducteur
CREATE TABLE conducteur(
    code VARCHAR(8) NOT NULL PRIMARY KEY,
    code_service VARCHAR(10) NOT NULL,
    nom VARCHAR(15) ,
    ville VARCHAR(15),
    salaire FLOAT,
    CONSTRAINT fk_service FOREIGN KEY (code_service)
    REFERENCES service(code_service)
);
-- table Marque
CREATE TABLE marque(
    code_marque VARCHAR(6) NOT NULL PRIMARY KEY,
    libelle VARCHAR(15)
);
-- table voiture
CREATE TABLE voiture(
    matricule VARCHAR(15) NOT NULL PRIMARY KEY,
    code_marque VARCHAR(6) NOT NULL,
    code_service VARCHAR(10) NOT NULL,
    puissance VARCHAR(15),
    type VARCHAR(15),
    CONSTRAINT fk_marque FOREIGN KEY (code_marque)
    REFERENCES marque(code_marque),
    CONSTRAINT fk_serice_voiture FOREIGN KEY (code_service)
    REFERENCES service(code_service)
);

-------------------------------

-- Q3 remplissage des tables
-- a : table service
INSERT INTO service(code_service,nom_service)
VALUES("serv001","Comptabilité");
INSERT INTO service(code_service,nom_service)
VALUES("serv002","Entretien");
INSERT INTO service(code_service,nom_service)
VALUES("serv003","Vente");
INSERT INTO service(code_service,nom_service)
VALUES("serv004","Informatique");
-- b : table marque
INSERT INTO marque(code_marque,libelle)
VALUES("marq1","Peuqeot");
INSERT INTO marque(code_marque,libelle)
VALUES("marq2","Fiat");
INSERT INTO marque(code_marque,libelle)
VALUES("marq3","Toyota");
INSERT INTO marque(code_marque,libelle)
VALUES("marq4","Renault");
-- c : table conducteur
INSERT INTO conducteur
VALUES("cond001","serv001","Salim","Agadir",27000);
INSERT INTO conducteur
VALUES("cond002","serv002","Mouhssine","Casablanca",3500);
INSERT INTO conducteur
VALUES("cond003","serv003","Yakoubi","Agadir",6000);
INSERT INTO conducteur
VALUES("cond004","serv003","Kasbi","Rabat",2800);
-- d : table voiture
INSERT INTO voiture
VALUES("33-a-205","marq1","serv001","7 chevaux","Gazoil");
INSERT INTO voiture
VALUES("06-a-1450","marq2","serv002","6 chevaux","Essence");
INSERT INTO voiture
VALUES("33-a-7458","marq4","serv001","7 chevaux","Gazoil");
INSERT INTO voiture
VALUES("06-a-1455","marq3","serv003","8 chevaux","Gazoil");
INSERT INTO voiture
VALUES("53-a-2004","marq1","serv001","7 chevaux","Gazoil");
INSERT INTO voiture
VALUES("06-a-8754","marq4","serv004","8 chevaux","Essence");
INSERT INTO voiture
VALUES("06-a-9877","marq2","serv003","6 chevaux","Gazoil");

-------------------------------
-- Q4

-------------------------------

-- Q5

SELECT * from conducteur 
WHERE ville="Agadir";

-------------------------------

-- Q6
SELECT matricule,puissance,type FROM voiture
JOIN marque 
ON marque.code_marque=voiture.code_marque
AND marque.libelle="fiat";

-------------------------------

-- Q7
SELECT * FROM conducteur
WHERE nom LIKE "%a%";

-------------------------------

-- Q8
SELECT * FROM conducteur
WHERE nom LIKE "_a%" AND nom LIKE "%b_";

-------------------------------

-- Q9
SELECT code,nom,salaire FROM conducteur
WHERE salaire > 5000;

-------------------------------

-- Q10
SELECT code,nom FROM conducteur
WHERE salaire BETWEEN 2000 and 3000 ;

