-- les fonctions intégrées de MYSQL
set @ville='Agadir'; -- variable local
select current_date() -- fonction intégrée(mysql)
    ,@@admin_port -- variable système
    ,concat('la ville :  ','@ville, /') -- fonction de Mysql
    ;
select abs(-4) -- fonction intégrée de Mysql
;
-- Créer une fonction qui retourne la somme de deux nombres

use Db01;
drop function if exists Fsomme;
delimiter $$ 
create function Fsomme(a float,b float) returns float
deterministic
begin
             declare somme float;
             set somme := (a+b);
             return somme;
end$$
-- Supprimer la fonction Fsomme
drop function Fsomme;
-- Utilisation de la fonction Fsomme
set @a=50;
set @z=28;
set @w= Fsomme(@a,@z);
select @w;
select @a as 'a',@z as 'b',
       Fsomme(@a,@z)  as 'somme';
-- créer une fonction qui affiche la moyenne 
-- des prix des livres
delimiter $$
create function FMoyenPrix() returns float
reads sql data
begin
   declare x float;
-- select avg(prix) into x from livre;
set x= (select avg(prix)  from livre);
 return x;
end $$
-- Execution de la fonction
select FMoyenPrix();
-- créer une fonction qui reçoit un pays 
-- puis affiche le nombre dauteur de ce pays;
delimiter $$
create function FnbreAuteurbycountry(country varchar(30))
         returns int
       reads sql data
begin
       declare nbre int;				
      select count(numaut)  into nbre 
      from auteur
      where pays=country; 
      return nbre;
end $$

-- Utiliser la fonction FnbreAuteurbycountry
select FnbreAuteurbycountry('italy');
-- Créer uen fonction qui ajoute un nouveau auteur
drop function if exists AjoutAuteur;
delimiter $$
create function AjoutAuteur(nameauteur varchar(30),country varchar(30)) returns int
deterministic    -- ou bien no sql  ou bien  reads sql data
begin
    insert into auteur (nom,pays) values(nameauteur,country);
        return 0;
end$$
-- Créer uen fonction qui supprime un  auteur
drop function if exists SupprimerAuteur;
delimiter $$
create function SupprimerAuteur(nameauteur varchar(30)) returns int
deterministic    -- ou bien no sql  ou bien  reads sql data
begin
    delete from  auteur where numaut=nameauteur;
        return 0;
end$$
-- supprimer l'auteur N° 6
select supprimerAuteur(6);

