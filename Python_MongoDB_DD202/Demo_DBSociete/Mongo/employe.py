from  pymongo import MongoClient

class Employe:
         cnx =MongoClient(host="localhost",port=27017)
         db=cnx["DB_societe"]
         # db=cnx.DB_societe
         collection=db["employes"]
         #collection = db.employes
         def __init__(self,id=0,nom="",grade="",ville="",salaire=0):
               self.id=id
               self.nom=nom
               self.grade=grade
               self.ville=ville
               self.salaire=salaire

         def allemploye(self):
             return    list(Employe.collection.find())

         def insertemploye(self,ref,name,grade,city,salary):
             Employe.collection.insert_one({"id":ref,"nom":name,"grade":grade,"ville":city,"salaire":salary})

         def editemploye(self,id):
               return list(Employe.collection.find({"id":id}))

         def deleteemploye(self,id_emp):
                Employe.collection.delete_many({"id":id_emp})

         def updateemploye(self,id,nom,grade,ville,salaire):
             Employe.collection.update_one({"id":id},{"$set":{"nom":nom,"grade":grade,"ville":ville,"salaire":salaire}})