#connection avec MongoDB
#importer la bibliothèque pymongo de python
from  pymongo import  MongoClient
#créer la connexion avec la base de données MongoDb
cnx=MongoClient(host="localhost",  port= 27017)
#accéder la base de données     "DB_DEV202_1"
db =cnx["DB_DEV202_1"]
#afficher le contenu de  la collection pilotes
collection_pilote=db["pilotes"]

def liste_pilote() :
              rows=list(collection_pilote.find())
              for r in rows :
                   print(f"num : {r['num']}   nom : {r['nom']} Ville ; {r['ville']}    Salaire : {r['salaire']}")

def  recherche_pilote(num):
         rows=list(collection_pilote.find({"num":num}))
         for r in rows:
              print(f"nom : {r['nom']} Ville ; {r['ville']}    Salaire : {r['salaire']}")
def  recherche_pilote_ville(ville) :
         #    rows=list(collection_pilote.find({"ville" : ville}))
         rows = list(collection_pilote.find())
         for r in rows:
                 if  str(r['ville']) .lower()== ville .lower():
                       print(f"nom : {r['nom']} Ville ; {r['ville']}    Salaire : {r['salaire']}")

def afficher_Total_salaire():
         x=list( collection_pilote.aggregate([{"$group": {"_id": 0, "sommesalaire": {"$sum": "$salaire"}}}]))
         print("****************************************")
         print(f"Total salaire :  {x[0]['sommesalaire']}")
         print("****************************************")
         col=list(collection_pilote.find({},{"_id": 0, "salaire":1}))
         somme=sum(map(lambda  x: x["salaire"],col))
         print(f"Total salaire :  {somme}")
         print("****************************************")

def ajouter_pilote(num,nom,ville,salaire):
       collection_pilote.insert_one({"num":num,"nom":nom,"ville":ville,"salaire":salaire})
       liste_pilote()


def supprimr_pilote(num):
          try :
               query = {"num": num}
               collection_pilote.delete_one(query)
          except:
                 print("suppression echouée!!!")
          liste_pilote()


def modifier_pilotes (num,nom,ville,salaire) :
         filter={"num":num}
         update={"nom":nom,"ville":ville,"salaire":salaire}
         collection_pilote.update_one(filter,{"$set":update})
         liste_pilote()


#Simulation
recherche_pilote_ville("agadir")
afficher_salaire()
#ajouter_pilote(5,"Errami","Rabat",24000)
afficher_salaire()
supprimr_pilote(2)
modifier_pilotes(3,"zeroual","Tanger",17000)